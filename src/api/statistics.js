import request from '@/utils/request'
import qs from 'qs'
//Statistics/getUsersAndSales
export function getUsersAndSales(data) {
    return request({
        url: '/Statistics/getUsersAndSales',
        method: 'get',
        params: data

    })
}

// Statistics/getSalesDetail
export function getSalesDetail(data) {
    return request({
        url: '/Statistics/getSalesDetail',
        method: 'get',
        params: data

    })
}

///Statistics/getSalesData
export function getSalesData(data) {
    return request({
        url: '/Statistics/getSalesData',
        method: 'get',
        params: data

    })
}

///Statistics/getNewUserCount
export function getNewUserCount(data) {
    return request({
        url: '/Statistics/getNewUserCount',
        method: 'get',
        params: data

    })
}

// Statistics/getPilesInfo
export function getPilesInfo(data) {
    return request({
        url: '/Statistics/getPilesInfo',
        method: 'get',
        params: data
    })
}

// Statistics/getRechargeDetail
export function getRechargeDetail(data) {
    return request({
        url: '/Statistics/getRechargeDetail',
        method: 'get',
        params: data
    })
}

// Statistics/getDayRecharge
export function getDayRecharge(data) {
    return request({
        url: '/Statistics/getDayRecharge',
        method: 'get',
        params: data
    })
}
// 导出excel
export function exportExcel(query) {
    return request({
        responseType: 'blob',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        transformRequest: [(params) => { return qs.stringify(params, { indices: false }) }],
        url: '/Statistics/export',
        method: 'get',
        params: query
    })
}

// Statistics/getDeviceInfo
export function getDeviceInfo(data) {
    return request({
        url: '/Statistics/getDeviceInfo',
        method: 'get',
        params: data
    })
}
