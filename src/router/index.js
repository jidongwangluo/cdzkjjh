import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [{
    path: '/login',
    component: () => import('@/views/login/indexNew'),
    hidden: true
},
{
    path: '/',
    component: Layout,
    redirect: '/home',
    meta: {
        title: '主页',
        icon: 'dashboard'
    },
    children: [{
        path: 'home',
        name: 'home',
        component: () => import('@/views/home'),
        meta: {
            title: '地图概览'
        },
    }]
}
]


export const asyncRouterMap = [
    {
        path: '/statistics',
        component: Layout,
        name: 'statistics',
        redirect: '/statistics',
        meta: {
            title: '统计',
            icon: 'tongji',
            noCache: true
        },
        children: [{
            path: '/statisticsList',
            name: 'statisticsList',
            component: () => import('@/views/statistics/userStatistics'),
            meta: {
                title: '数据统计'
            }
        },
        {
            path: '/dayRecharge',
            name: 'dayRecharge',
            component: () => import('@/views/statistics/dayRecharge'),
            hidden: true,
            meta: {
                title: '充值记录',
                activeMenu: '/statistics/statisticsList'
            }
        }]
    }, {
        path: '/device',
        component: Layout,
        name: 'device',
        redirect: 'noRedirect',
        meta: {
            title: '设备管理',
            icon: 'shebei'
        },
        children: [{
            path: 'deviceList',
            name: 'deviceList',
            component: () => import('@/views/device/deviceList'),
            meta: {
                title: '充电桩管理'
            }
        }, {
            path: 'portList',
            name: 'portList',
            component: () => import('@/views/device/portList'),
            hidden: true,
            meta: {
                title: '端口管理',
                activeMenu: '/device/deviceList'
            }
        },
        {
            path: 'plotList',
            name: 'plotList',
            component: () => import('@/views/device/plotList'),
            meta: {
                title: '站点管理'
            }
        }
        ]
    }, {
        path: '/order',
        component: Layout,
        // redirect: '/rules',
        children: [{
            path: '/orderList',
            component: () => import('@/views/order/orderList'),
            meta: {
                title: '订单管理',
                icon: 'dingdan'
            }
        }, {
            path: 'orderDetail',
            component: () => import('@/views/order/orderDetail'),
            hidden: true,
            meta: {
                title: '订单详情',
                activeMenu: '/orderList'
            }
        }]
    }, {
        path: '/card',
        component: Layout,
        redirect: '/card/cardList',
        children: [{
            path: '/cardList',
            component: () => import('@/views/card/list'),
            meta: {
                title: 'IC卡管理',
                icon: 'ic_Profiles'
            }
        }, {
            path: 'useRecods',
            component: () => import('@/views/card/useRecods'),
            hidden: true,
            meta: {
                title: '使用记录',
                activeMenu: '/cardList'
            }
        }]
    },
    {
        path: '/rulesmanage',
        component: Layout,
        redirect: 'noRedirect',
        meta: {
            title: '规则管理',
            icon: 'guize'
        },
        children: [{
            path: '/rules',
            component: () => import('@/views/rules/index'),
            redirect: '/rules/list',
            meta: {
                title: '规则列表',
                icon: 'nested'
            },
            children: [
                {
                    path: '/rules/list',
                    component: () => import('@/views/rules/list'),
                    hidden: true,
                    meta: {
                        title: '',
                        icon: 'nested'
                    }
                },
                {
                    path: 'price',
                    component: () => import('@/views/rules/price/price'),
                    hidden: true,
                    meta: {
                        title: '价格配置',
                        activeMenu: '/rules'
                    }
                }
            ]
        }]
    }, {
        path: '/registerUser',
        component: Layout,
        redirect: '/registerUser',
        children: [{
            path: '/registerUser',
            component: () => import('@/views/registerUser/userList'),
            meta: {
                title: '注册用户',
                icon: 'yonghu'
            }
        },
        {
            path: 'chongzhiRecods',
            component: () => import('@/views/registerUser/chongzhiRecods'),
            hidden: true,
            meta: {
                title: '充值记录',
                activeMenu: '/registerUser'
            }
        }
        ]
    },
    {
        path: '/operator',
        component: Layout,
        redirect: '/operator',
        children: [{
            path: '/operatorList',
            component: () => import('@/views/operator/operatorList'),
            meta: {
                title: '代理商管理',
                icon: 'daili'
            }
        },
        {
            path: 'tixianRecods',
            component: () => import('@/views/operator/tixianRecods'),
            hidden: true,
            meta: {
                title: '提现记录',
                activeMenu: '/operator'
            }
        },
        {
            path: "/childrenAccount",
            component: () => import('@/views/operator/childrenAccount'),
            hidden: true,
            meta: {
                title: '代理商详情',
            }
        }
        ]
    },
    {
        path: '/systemUser',
        component: Layout,
        name: 'systemUser',
        // redirect: 'reserveManagement/reserveInfo',
        redirect: 'noRedirect',
        meta: {
            title: '系统管理',
            icon: 'nested'
        },
        children: [{
            path: 'role',
            name: 'role',
            component: () => import('@/views/systemUser/roleList'),
            meta: {
                title: '岗位管理'
            }
        }, {
            path: 'user',
            name: 'user',
            component: () => import('@/views/systemUser/userList'),
            meta: {
                title: '用户管理'
            }
        }, {
            path: 'menu',
            name: 'menu',
            component: () => import('@/views/systemUser/menuList'),
            meta: {
                title: '菜单管理'
            }
        }
            // ,
            // {
            //     path: 'data',
            //     name: 'data',
            //     component: () => import('@/views/systemUser/userOperators'),
            //     meta: {
            //         title: '数据权限'
            //     }
            // }
        ]
    },
    {
        path: '/feedback',
        component: Layout,
        redirect: '/feedbackList',
        children: [{
            path: '/feedbackList',
            component: () => import('@/views/feedback/feedbackList'),
            meta: {
                title: '反馈管理',
                icon: 'fankui'
            }
        }]
    },
    {
        path: '/advert',
        component: Layout,
        redirect: '/advertList',
        children: [{
            path: '/advertList',
            component: () => import('@/views/advert/advertList'),
            meta: {
                title: '广告管理',
                icon: 'guanggao'
            }
        }]
    },
    {
        path: '/promotion',
        component: Layout,
        redirect: '/promotionList',
        children: [{
            path: '/promotionList',
            component: () => import('@/views/promotion/promotionList'),
            meta: {
                title: '促销管理',
                icon: 'cuxiao'
            }
        }]
    },
    {
        path: '/finance',
        component: Layout,
        redirect: '/financeList',
        children: [{
            path: '/financeList',
            component: () => import('@/views/finance/financeList'),
            meta: {
                title: '财务管理',
                icon: 'caiwu'
            }
        }]
    },
    {
        path: '/accountChildren',
        component: Layout,
        redirect: '/accountChildren',
        children: [{
            path: '/accountChildren',
            component: () => import('@/views/accountChildren'),
            meta: {
                title: '分成者管理',
                icon: 'caiwu'
            }
        }]
    },
    {
        path: '*',
        redirect: '/',
        hidden: true
    }
];
const createRouter = () => new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({
        y: 0
    }),
    routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
}
export default router
